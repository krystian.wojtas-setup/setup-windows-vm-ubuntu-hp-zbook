#!/bin/bash

# Initialize custom setup
# Manually steps to run before this script:
#   - update packages list
#       sudo apt update
#   - upgrade system
#       sudo apt upgrade --yes
#   - install build-essential and git packages
#       sudo apt install --yes build-essential git
#   - restart vm to have newest kernel
#       sudo reboot
#   - mount virtual machine guest additions
#   - install virtual machine guest additions
#       sudo /media/$USER/*/VBoxLinuxAdditions.run
#   - install vmware player
#       download from www.vmware.com/go/downloadplayer
#       sudo ./VMware-Player-15.1.0-13591040.x86_64.bundle

# Stop on first failure
set -e

# Show each command
set -x

# Go to script directory
cd "$(realpath "$(dirname "$0")")"

# Define function to create symbolic links
install_symlink()
{
    # Get target file path for symbolic link
    target="$1"

    # Create symbolic link
    ln \
        --symbolic \
        \
        `# Overwrite if symlink already exists` \
        --force \
        \
        `# Target of symlink` \
        "$HOME/root/$target" \
        \
        `# Symlink path itself` \
        "$target"
}

# Add Microsoft repository key
wget \
    --quiet \
    --output-document \
        - \
    https://packages.microsoft.com/keys/microsoft.asc \
|
apt-key \
    add \
        - \

# Add Microsoft repostitory pacakges list
wget \
    --quiet \
    --output-document \
        /etc/apt/sources.list.d/microsoft.list \
    https://packages.microsoft.com/config/ubuntu/18.04/prod.list

# Update the list of software packages
apt-get update

# Install some extra software packages
apt-get \
    install \
        --yes \
        \
        arandr \
        build-essential \
        chromium-browser \
        cloc \
        curl \
        docker.io \
        emacs \
        expect \
        git \
        i3 \
        meld \
        mosh \
        nmap \
        npm \
        openconnect \
        openjdk-11-jdk \
        openssh-server \
        powershell \
        picocom \
        python-pygments \
        sshpass \
        strongswan-swanctl \
        tig \
        tmux \
        tree \
        vagrant \
        xclip \
        xsel \
        zsh \

# Install custom user configuration before creating symlinks to it
`# Run command as target user` \
su \
    "$SUDO_USER" \
    --command \
        ./install

# Install symbolic links to system configuration files
install_symlink /etc/default/motd-news
install_symlink /etc/hostname
install_symlink /etc/ssh/sshd_config
install_symlink /etc/resolv.conf.9.9.9.9
install_symlink /etc/systemd/network/10-eth0.link
install_symlink /usr/share/bash-completion/completions/tig_completion

# Vagrant from ubuntu repository is broken
wget \
    --quiet \
    https://releases.hashicorp.com/vagrant/2.2.4/vagrant_2.2.4_x86_64.deb
dpkg \
    --install \
    vagrant_2.2.4_x86_64.deb
rm vagrant_2.2.4_x86_64.deb

# Install newest packer
wget \
    --quiet \
    https://releases.hashicorp.com/packer/1.4.1/packer_1.4.1_linux_amd64.zip
unzip packer_1.4.1_linux_amd64.zip
mv packer "$HOME/bin/"
rm packer_1.4.1_linux_amd64.zip

# Add user to some groups
usermod --append --groups docker "$SUDO_USER"
usermod --append --groups vboxsf "$SUDO_USER"

# Set zsh as default shell for target user
chsh --shell /bin/zsh "$SUDO_USER"

# Install mermaid globally
npm install -g mermaid.cli

# Install VMware Powercli module for powershell
# Agree for untrusted repository source
yes | pwsh -Command 'Install-Module -Name VMware.PowerCLI'

# Do not display VMware warning about Customer Experience Improvement Program
yes | pwsh -Command 'Set-PowerCLIConfiguration -Scope User -ParticipateInCEIP $false'

# Ignore message about invalid certificate
yes | pwsh -Command 'Set-PowerCLIConfiguration -InvalidCertificateAction Ignore'

# Install perl module to read and write files
yes | cpan install File::Slurp

# Regenerate initramfs to renaming network interface have effect
# then reboot
update-initramfs -u
